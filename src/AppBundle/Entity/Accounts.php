<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Accounts
 *
 * @ORM\Table(name="accounts", indexes={@ORM\Index(name="AccountID", columns={"AccountID"})})
 * @ORM\Entity
 */
class Accounts
{
    /**
     * @var string
     *
     * @ORM\Column(name="AccountUsername", type="string", length=255, nullable=true)
     */
    private $accountusername;

    /**
     * @var string
     *
     * @ORM\Column(name="AccountPassword", type="string", length=255, nullable=true)
     */
    private $accountpassword;

    /**
     * @var string
     *
     * @ORM\Column(name="IGN", type="string", length=255, nullable=true)
     */
    private $ign;

    /**
     * @var integer
     *
     * @ORM\Column(name="AccountID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $accountid;

public function setAccountID($accountID)
    {
        $this->accountid = $accountID;

        return $this;
    }

    public function getAccountID()
    {
        return $this->accountid;
    }

    public function setAccountUsername($accountUsername)
    {
        $this->accountusername = $accountUsername;

        return $this;
    }

    public function getAccountUsername()
    {
        return $this->accountusername;
    }

    public function setAccountPassword($accountPassword)
    {
        $this->accountpassword = $accountPassword;

        return $this;
    }

    public function getAccountPassword()
    {
        return $this->accountpassword;
    }

    public function setIgn($ign)
    {
        $this->ign = $ign;

        return $this;
    }

    public function getIgn()
    {
        return $this->ign;
    }



}

