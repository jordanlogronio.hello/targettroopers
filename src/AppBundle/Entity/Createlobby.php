<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Createlobby
 *
 * @ORM\Table(name="createlobby", indexes={@ORM\Index(name="LobbyID", columns={"LobbyID"})})
 * @ORM\Entity
 */
class Createlobby
{
    /**
     * @var string
     *
     * @ORM\Column(name="LobbyPassword", type="string", length=255, nullable=true)
     */
    private $lobbypassword;

    /**
     * @var string
     *
     * @ORM\Column(name="CreatedBy", type="string", length=255, nullable=true)
     */
    private $createdby;

    /**
     * @var string
     *
     * @ORM\Column(name="Joined", type="string", length=255, nullable=true)
     */
    private $joined;

    /**
     * @var string
     *
     * @ORM\Column(name="PlayerOneData", type="text", length=65535, nullable=true)
     */
    private $playeronedata;

    /**
     * @var string
     *
     * @ORM\Column(name="PlayerTwoData", type="text", length=65535, nullable=true)
     */
    private $playertwodata;

    /**
     * @var string
     *
     * @ORM\Column(name="PlayerOneTroops", type="string", length=255, nullable=true)
     */
    private $playeronetroops;

    /**
     * @var string
     *
     * @ORM\Column(name="PlayerTwoTroops", type="string", length=255, nullable=true)
     */
    private $playertwotroops;

    /**
     * @var integer
     *
     * @ORM\Column(name="PlayerField", type="integer", nullable=true)
     */
    private $playerfield;

    /**
     * @var string
     *
     * @ORM\Column(name="isDisconnect", type="string", length=45, nullable=true)
     */
    private $isdisconnect;

    /**
     * @var integer
     *
     * @ORM\Column(name="LobbyID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $lobbyid;

    public function getLobbyID()
    {
        return $this->lobbyid;
    }

    public function setLobbyID($LobbyID)
    {
        $this->lobbyid = $LobbyID;

        return $this;
    }

    public function getLobbyPassword()
    {
        return $this->lobbypassword;
    }

    public function setLobbyPassword($LobbyPassword)
    {
        $this->lobbypassword = $LobbyPassword;

        return $this;
    } 
    public function getCreatedBy()
    {
        return $this->createdby;
    }

    public function setCreateBy($CreateBy)
    {
        $this->createdby = $CreateBy;

        return $this;
    } 
    public function getJoined()
    {
        return $this->joined;
    }

    public function setJoined($Joined)
    {
        $this->joined = $Joined;

        return $this;
    }   
    public function getPlayerOneData()
    {
        return $this->playeronedata;
    }

    public function setPlayerOneData($PlayerOneData)
    {
        $this->playeronedata = $PlayerOneData;

        return $this;
    }
    public function getPlayerTwoData()
    {
        return $this->playertwodata;
    }

    public function setPlayerTwoData($PlayerTwoData)
    {
        $this->playertwodata = $PlayerTwoData;

        return $this;
    }  

    public function getPlayerOneTroops()
    {
        return $this->playeronetroops;
    }

    public function setPlayerOneTroops($PlayerOneTroops)
    {
        $this->playeronetroops = $PlayerOneTroops;

        return $this;
    }  

    public function getPlayerTwoTroops()
    {
        return $this->playertwotroops;
    }

    public function setPlayerTwoTroops($PlayerTwoTroops)
    {
        $this->playertwotroops = $PlayerTwoTroops;

        return $this;
    }  


    public function getPlayerField()
    {
        return $this->playerfield;
    }

    public function setPlayerField($PlayerField)
    {
        $this->playerfield = $PlayerField;

        return $this;
    }  

    public function getIsDisconnect()
    {
        return $this->isdisconnect;
    }

    public function setIsDisconnect($isdisconnect)
    {
        $this->isdisconnect = $isdisconnect;

        return $this;
    }  
}

