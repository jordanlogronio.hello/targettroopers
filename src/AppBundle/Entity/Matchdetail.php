<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Matchdetail
 *
 * @ORM\Table(name="matchdetail", indexes={@ORM\Index(name="MatchID", columns={"MatchID"})})
 * @ORM\Entity
 */
class Matchdetail
{
    /**
     * @var integer
     *
     * @ORM\Column(name="LobbyID", type="integer", nullable=true)
     */
    private $lobbyid;

    /**
     * @var integer
     *
     * @ORM\Column(name="MatchID", type="integer", nullable=true)
     */
    private $matchid;

    /**
     * @var integer
     *
     * @ORM\Column(name="PlayerID", type="integer", nullable=true)
     */
    private $playerid;

    /**
     * @var integer
     *
     * @ORM\Column(name="DataTop", type="integer", nullable=true)
     */
    private $datatop;

    /**
     * @var integer
     *
     * @ORM\Column(name="DataLeft", type="integer", nullable=true)
     */
    private $dataleft;

    /**
     * @var string
     *
     * @ORM\Column(name="damageTroopspOne", type="text", length=65535, nullable=true)
     */
    private $damagetroopspone;

    /**
     * @var string
     *
     * @ORM\Column(name="damageTroopspTwo", type="text", length=65535, nullable=true)
     */
    private $damagetroopsptwo;

    /**
     * @var integer
     *
     * @ORM\Column(name="PointerID", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="IDENTITY")
     */
    private $pointerid;

public function getPointerID()
    {
        return $this->pointerid;
    }

    public function setPointerID($PointerID)
    {
        $this->pointerid = $PointerID;
    }

     public function getLobbyID()
    {
        return $this->lobbyid;
    }

    public function setLobbyID($LobbyID)
    {
        $this->lobbyid = $LobbyID;

    }
    public function getMatchID()
    {
        return $this->matchid;
    }

    public function setMatchID($MatchID)
    {
        $this->matchid = $MatchID;

    }
    public function getPlayerID()
    {
        return $this->playerid;
    }

    public function setPlayerID($PlayerID)
    {
        $this->playerid = $PlayerID;

    }
     public function getDataTop()
    {
        return $this->datatop;
    }

    public function setDataTop($Top)
    {
        $this->datatop = $Top;

    }
     public function getDataLeft()
    {
        return $this->dataleft;
    }

    public function setDataLeft($Left)
    {
        $this->dataleft = $Left;

    }
     public function getDamageTroopspOne()
    {
        return $this->damagetroopspone;
    }

    public function setDamageTroopsPone($damagetroopspone)
    {
        $this->damagetroopspone = $damagetroopspone;
    }

    public function getDamageTroopspTwo()
    {
        return $this->damagetroopsptwo;
    }

    public function setDamageTroopsPtwo($damagetroopsptwo)
    {
        $this->damagetroopsptwo = $damagetroopsptwo;
    }  


}

