<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\JsonResponse;
use AppBundle\Entity\Accounts;
use AppBundle\Entity\Createlobby;
use AppBundle\Entity\Matchdetail;
use Symfony\Component\HttpFoundation\Session\Session;


class DefaultController extends Controller
{

    private $session;

    public function __construct()
    {
        $this->session = new Session();
    }

    /**
     * @Route("/", name="homepage")
     */
    public function indexAction(Request $request)
    {
        $checker = $this->session->get("ifLogin");
        if(!$checker){
            return $this->redirectToRoute("login");               
        }
         return $this->render('default/login.html.twig');   
    }

    /**
     * @Route("/login", name="login")
     */
    public function login(Request $request)
    {   
        $checker = $this->session->get("ifLogin");
        if($checker){
            return $this->redirectToRoute("lobby");               
        }
         return $this->render('default/login.html.twig');   
    }


    //Content 

    /**
    *@Route("/disconnect/{id}")
    */
    Public function disconnect(Request $request,$id)
    {

       $em = $this->getDoctrine()->getManager();
    
        $updatedata=$em->getRepository(Createlobby::class)->Find($id);
         $updatedata->setIsDisconnect("disconnected");   
        $em->flush();
         
      return $this->redirectToRoute("lobby");     
             
    }
        /**
    *@Route("/logout", name="logout")
    */
    Public function logout(Request $request)
    {
        $this->session->clear();

      return $this->redirectToRoute("login");     
             
    }
    /**
     * @Route("/lobby", name="lobby")
     */
    public function lobby(Request $request)
    {
        $checker = $this->session->get("ifLogin");
        if(!$checker){
            return $this->redirectToRoute("login");               
        }
        $em = $this->getDoctrine()->getManager();
        $data =$em->createQuery("SELECT u FROM AppBundle:Createlobby u WHERE u.joined = '' and u.isdisconnect = 'default'")
            ->getResult();

        $getign = [];
        for($x=0 ; $x < sizeof($data); $x++){
        $ign =$em->createQuery("SELECT u FROM AppBundle:Accounts u WHERE u.accountid = :lb ")
            ->setParameter('lb',$data[$x]->getCreatedBy())
            ->getResult();

            $getign[$x] = $ign[0]->getIgn();
         }

         
          return $this->render('default/homepage.html.twig',['data' => $data,'ign' => $getign,'ignuser' => $this->session->get('ign')]);
    
    }

     /**
     * @Route("/game")
     */
    public function game(Request $request)
    {
        $checker = $this->session->get("ifLogin");
        if(!$checker){
            return $this->redirectToRoute("login");               
        }
            $em = $this->getDoctrine()->getManager();
            $checkifready =$em->createQuery("SELECT u FROM AppBundle:Createlobby u WHERE u.lobbyid = :lb ")
                ->setParameter("lb",$this->session->get("lobbyid"))
                ->getResult();

          $p1 =  $checkifready[0]->getPlayerOneData();
          $p2 =  $checkifready[0]->getPlayerTwoData();
          $p1Troops = $checkifready[0]->getPlayerOneTroops();
          $p2Troops = $checkifready[0]->getPlayerTwoTroops();

        $PlayerOneDataCut = explode(":", $p1);

        for ($i=0; $i < sizeof($PlayerOneDataCut); $i++) { 
                $RealPlayerOneData[$i] = explode(" ", $PlayerOneDataCut[$i]);       
        }

        for($x = 0 ; $x < sizeof($RealPlayerOneData)-1 ; $x++){
            $p1Top[$x] = $RealPlayerOneData[$x][0];
            $p1Left[$x] = $RealPlayerOneData[$x][1];
            $p1CloseBtn[$x] = $RealPlayerOneData[$x][2];            
        }

        $PlayerTwoDataCut = explode(":", $p2);

        for ($i=0; $i < sizeof($PlayerTwoDataCut); $i++) { 
                $RealPlayerTwoData[$i] = explode(" ", $PlayerTwoDataCut[$i]);       
        }

        for($x = 0 ; $x < sizeof($RealPlayerTwoData)-1 ; $x++){
            $p2Top[$x] = $RealPlayerTwoData[$x][0];
            $p2Left[$x] = $RealPlayerTwoData[$x][1];
            $p2CloseBtn[$x] = $RealPlayerTwoData[$x][2];            
        }  

    $cnt = 0;
    $contentp1 = "";
    for($x = 0 ; $x < sizeof($RealPlayerOneData)-1 ; $x++){

    if($this->session->get("playertype") == "p1"){
    $contentp1 .= '<div id="divp1'.$cnt.'">'.
        '<img id="troopsp1'.$x.'" src="'.$this->session->get("imgtroops").'" style="position:fixed; top:'.$p1Top[$x].'px; left:'.$p1Left[$x].'px;"/></div>';
    }
    else{
    $contentp1 .= '<div id="divp1'.$cnt.'">'.
        '<img id="troopsp1'.$x.'" src="'.$this->session->get("imgtroops").'" style="opacity:0;position:fixed; top:'.$p1Top[$x].'px; left:'.$p1Left[$x].'px;"/></div>';        
    }
    $cnt++;
    }


    $cnt = 0;
    $contentp2 = "";
    for($x = 0 ; $x < sizeof($RealPlayerTwoData)-1 ; $x++){

    if($this->session->get("playertype") == "p1"){
    $contentp2 .= '<div id="divp2'.$cnt.'">'.
        '<img id="troopsp2'.$x.'" src="'.$this->session->get("imgtroops").'" style="opacity:0; position:fixed; top:'.$p2Top[$x].'px; left:'.$p2Left[$x].'px;"/></div>';
    }
    else{
    $contentp2 .= '<div id="divp2'.$cnt.'">'.
        '<img id="troopsp2'.$x.'" src="'.$this->session->get("imgtroops").'" style="position:fixed; top:'.$p2Top[$x].'px; left:'.$p2Left[$x].'px;"/></div>';        
    }
    $cnt++;
    }

       return $this->render('default/game.html.twig',
        ['lobbyid' => $this->session->get("lobbyid"),
        'playertype' => $this->session->get("playertype"),
        'p1Top' => $p1Top,
        'p1Left' => $p1Left,
        'p2Top' => $p2Top,
        'p2Left' => $p2Left,
        'contentp1' => $contentp1,
        'contentp2' => $contentp2,
        'imgp1' => $p1Troops,
        'imgp2' => $p2Troops,
        'ignp1' => $this->session->get("ignp1"),
        'ignp2' => $this->session->get("ignp2"),
        'field' => $checkifready[0]->getPlayerField()
        ]);
    }
    




    /**
     * @Route("/Createlobby")
     */
    public function createlobby(Request $request)
    {
        $checker = $this->session->get("ifLogin");
        if(!$checker){
            return $this->redirectToRoute("login");               
        }
      $em = $this->getDoctrine()->getManager();
        $lobbyid =$em->createQuery("SELECT cl FROM AppBundle:Createlobby cl WHERE cl.createdby = :lb")
            ->setParameter("lb",$this->session->get("accountid"))
            ->getResult();
      $this->session->set("lobbyid",$lobbyid[sizeof($lobbyid)-1]->getLobbyID());        
      return $this->render('default/createlobby.html.twig',['lobbyid' => $lobbyid[sizeof($lobbyid)-1]->getLobbyID()]);
       
    }
    /**
     * @Route("/placetroops")
     */
    public function placetroops(Request $request)
    {
        $checker = $this->session->get("ifLogin");
        if(!$checker){
            return $this->redirectToRoute("login");               
        }
          $em = $this->getDoctrine()->getManager();
    
            $data =$em->createQuery("SELECT u FROM AppBundle:Createlobby u WHERE u.lobbyid = :lb ")
            ->setParameter('lb',$this->session->get("lobbyid"))
            ->getResult();


        $ignp1 =$em->createQuery("SELECT u FROM AppBundle:Accounts u WHERE u.accountid = :lb ")
            ->setParameter('lb',$data[0]->getCreatedBy())
            ->getResult();

            $getignp1 = $ignp1[0]->getIgn();
            $this->session->set("ignp1",$getignp1);
         
        $ignp2 =$em->createQuery("SELECT u FROM AppBundle:Accounts u WHERE u.accountid = :lb ")
            ->setParameter('lb',$data[0]->getJoined())
            ->getResult();

            $getignp2 = $ignp2[0]->getIgn();
            $this->session->set("ignp2",$getignp2);

      return $this->render('default/placetroops.html.twig',['lobbyid' => $this->session->get("lobbyid"),'playertype' => $this->session->get("playertype"),'img' => $this->session->get("imgtroops"),'ignp1' => $getignp1,'ignp2' => $getignp2,'field' => $data[0]->getPlayerField()]);
       
    }

    /**
     * @Route("/choosetroops")
     */
    public function choosetroops(Request $request)
    {
        $checker = $this->session->get("ifLogin");
        if(!$checker){
            return $this->redirectToRoute("login");               
        }
        
      return $this->render('default/choosetroops.html.twig',['lobbyid' => $this->session->get('lobbyid'),'playertype' => $this->session->get('playertype')]);
       
    }


    //AJAX




    /**
     * @Route("/checkifdisconnected")
     */
    public function checkifdisconnected(Request $request)
    {

        $em = $this->getDoctrine()->getManager();
        $updatedata=$em->getRepository(Createlobby::class)->Find($request->request->get('LobbyID'));
        
        if($updatedata->getIsDisconnect() == "disconnected"){
            $message = true;
        }
        else{
            $message = false;
        }

            return new JsonResponse($message);
    }

    /**
     * @Route("/checkturn")
     */
    public function checkturn(Request $request)
    {
      $em = $this->getDoctrine()->getManager();
        $Matchdetail =$em->createQuery("SELECT cl FROM AppBundle:Matchdetail cl WHERE cl.lobbyid = :lb")
            ->setParameter("lb",$request->request->get('LobbyID'))
            ->getResult();
            $message = [];

            if(empty($Matchdetail)){
                $message['message'] = "even"; 
                $message['MatchID'] = 0;
            }
            else{
            $message['MatchID'] = $Matchdetail[sizeof($Matchdetail)-1]->getMatchID();
            $message['PlayerID'] = $Matchdetail[sizeof($Matchdetail)-1]->getPlayerID();
            $message['Top'] = $Matchdetail[sizeof($Matchdetail)-1]->getDataTop();
            $message['Left'] = $Matchdetail[sizeof($Matchdetail)-1]->getDataLeft();
            $message['damageTroopsp1'] = $Matchdetail[sizeof($Matchdetail)-1]->getDamageTroopspOne();
            $message['damageTroopsp2'] = $Matchdetail[sizeof($Matchdetail)-1]->getDamageTroopspTwo();
                if($message['MatchID'] % 2 == 0 ){
                    $message['message'] = "even";
                }
                else{
                    $message['message'] = "odd";
                }    
            } 



            return new JsonResponse($message);
    }


    /**
     *@Route("/insertturndata")
     */
    public function insertturndata(Request $request)
    {   
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        else{
            $message="";


            $em = $this->getDoctrine()->getManager();
            $md = new Matchdetail();

            $md->setPointerID(0);                
            $md->setLobbyID($request->request->get('LobbyID'));
            $md->setMatchID($request->request->get('MatchID'));
            $md->setPlayerID($this->session->get("accountid"));
            $md->setDataTop($request->request->get('Top'));
            $md->setDataLeft($request->request->get('Left'));
            $md->setDamageTroopspOne($request->request->get('damageTroopsp1'));
            $md->setDamageTroopspTwo($request->request->get('damageTroopsp2'));

            $em->persist($md);
            $em->flush();
            
            return new JsonResponse($md->getDamageTroopspTwo());
        }       
    }

    /**
     *@Route("/sendimgtroops")
     */
    public function sendimgtroops(Request $request)
    {   
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        else{
               
           $this->session->set("imgtroops",$request->request->get('imgtroops'));
          $em = $this->getDoctrine()->getManager();
    
        $updatedata=$em->getRepository(Createlobby::class)->Find($request->request->get('LobbyID'));
            if($request->request->get("playertype") == "p1"){
                $updatedata->setPlayerOneTroops($request->request->get('imgtroops'));
            }
            else{
                $updatedata->setPlayerTwoTroops($request->request->get('imgtroops'));
            }
            
            $em->flush();
            return new JsonResponse($this->session->get('imgtroops'));
        }       
    }

    /**
     *@Route("/checkifready")
     */
    public function checkifready(Request $request)
    {   
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        else{
            $message="";
            $em = $this->getDoctrine()->getManager();
            $checkifready =$em->createQuery("SELECT u FROM AppBundle:Createlobby u WHERE u.lobbyid = :lb ")
                ->setParameter("lb",$request->request->get('LobbyID'))
                ->getResult();

            if(!empty($checkifready[0]->getPlayerOneData()) && !empty($checkifready[0]->getPlayerTwoData())){
                $message = "Ready";
            }
            else{
                $message = "NotReady";
            }
            
            return new JsonResponse($message);
        }       
    }


    /**
     *@Route("/sendplayerdata")
     */
    public function sendplayerdata(Request $request)
    {   
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        else{
            $message="";
            $em = $this->getDoctrine()->getManager();
            $updatedata=$em->getRepository(Createlobby::class)->Find($request->request->get('LobbyID'));
            if($request->request->get("type") == "p1"){
                $updatedata->setPlayerOneData($request->request->get('data'));
                $message ="Ready";
            }
            else{
                $updatedata->setPlayerTwoData($request->request->get('data'));
                $message ="Ready";
            }

            $em->flush();
            
            return new JsonResponse($message);
        }
       
    }

    /**
     *@Route("/Checkifuserconnected")
     */
    public function Checkifuserconnected(Request $request)
    {   
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        else{
            $em = $this->getDoctrine()->getManager();
            $joined =$em->createQuery("SELECT cl FROM AppBundle:Createlobby cl WHERE cl.lobbyid = :lb")
                ->setParameter("lb",$request->request->get('LobbyID'))
                ->getResult();

            if(empty($joined[0]->getJoined())){
            $message = "";
            }
            else{
            $message = "Joined";
            }
            return new JsonResponse($message);
        
        }
       
    }



    /**
     * @Route("/ajaxRequestLobby")
     */
    public function sendDataTolobby(Request $request)
    {
        
        if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        else{
            $message = "";
            $em = $this->getDoctrine()->getManager();


            if($request->request->get('LobbyID')=="createlobby"){
            $Createlobby = new Createlobby();
            
            $Createlobby->setLobbyID(0);
            $Createlobby->setLobbyPassword($request->request->get('password'));
            $Createlobby->setCreateBy($this->session->get("accountid"));
            $Createlobby->setJoined("");
            $Createlobby->setPlayerOneData("");
            $Createlobby->setPlayerTwoData("");

            $rand = rand(1,4);
            $Createlobby->setPlayerField($rand);
            $Createlobby->setIsDisconnect("default");


            $em->persist($Createlobby);
            $em->flush();
            $message = "create";
            $this->session->set("playertype","p1");
                    
            }
            else{

            $Createlobby=$em->getRepository(Createlobby::class)->Find($request->request->get('LobbyID'));
                if( !empty($Createlobby->getJoined()) ){
                $message ="alreadytake";
                }
                else{
                    $Createlobby->setJoined($this->session->get("accountid"));

                    $em->flush();
                    $message ="joined";
                    $this->session->set("playertype","p2");
                    $this->session->set("lobbyid",$request->request->get('LobbyID'));
                }
            }

        return new JsonResponse($message);
        }   
    }



    /**
     * @Route("/ajaxRequestLogin")
     */
    public function ajaxLogin(Request $request)
    {

         if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        else{

         $em = $this->getDoctrine()->getManager();
        $Accounts =$em->createQuery("SELECT u FROM AppBundle:Accounts u WHERE u.accountusername = :username AND u.accountpassword = :password")
            ->setParameter("username", $request->request->get('username'))
            ->setParameter("password", $request->request->get('password'))
            ->getResult();

        if(empty($Accounts)){
            $arrData = ['message' => "Not Exist"];
        }
        else{
            $this->session->set("ifLogin",true);
            $this->session->set("accountid",$Accounts[0]->getAccountID());
            $this->session->set("ign",$Accounts[0]->getIgn());
            $arrData = ['message' => "Exist"];
        }

        return new JsonResponse($arrData);
        }

    }

    /**
     * @Route("/ajaxRequestSignup")
     */
    public function ajaxRequestSignup(Request $request)
    {

         if (!$request->isXmlHttpRequest()) {
            return new JsonResponse(array('message' => 'Use only ajax please!'), 400);
        }
        else{

         $em = $this->getDoctrine()->getManager();
        $Accounts =$em->createQuery("SELECT u FROM AppBundle:Accounts u WHERE u.accountusername = :username")
            ->setParameter("username", $request->request->get('username'))
            ->getResult();

        if(!empty($Accounts)){
            $arrData = ['message' => "Exist"];
        }
        else{
            $ac = new Accounts();
            
            $ac->setAccountID(0);
            $ac->setAccountUsername($request->request->get('username'));
            $ac->setAccountPassword($request->request->get('password'));
            $ac->setIgn($request->request->get('ign'));

            $em->persist($ac);
            $em->flush();
            $arrData = ['message' => "success"];
        }

        return new JsonResponse($arrData);
        }

    }
        


        /**
     * @Route("/try/{id}")
     */
    public function try(Request $request,$id)
    {


                dump($id);
            die();
            return new JsonResponse($message);

    }
}//end
